package prvnimilion.vutindex.ui_common.enums

enum class DifferenceType {
    POINTS, CREDIT, PASSED, MESSAGE
}