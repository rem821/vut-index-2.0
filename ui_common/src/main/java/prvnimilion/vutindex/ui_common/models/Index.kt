package prvnimilion.vutindex.ui_common.models

data class Index(val semesters: MutableList<Semester>)