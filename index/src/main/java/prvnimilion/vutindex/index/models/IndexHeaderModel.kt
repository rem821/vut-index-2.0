package prvnimilion.vutindex.index.models

data class IndexHeaderModel(
    val id: Int,
    val header: String
) : IndexFeedModel